const express = require("express");
const app = express();

app.get("/", (req,res) => {
    res.send({ hello: "world" });
});

app.get("/sum", (req,res) => {
    var A = parseInt(req.query.A);
    var B = parseInt(req.query.B);
    var result = A + B;
    res.send({ sum: result });
});

const PORT = process.env.PORT || 5000;
app.listen(PORT, function(){
    console.log("App listening on port ${PORT}");
});
